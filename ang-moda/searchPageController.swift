//
//  searchPageController.swift
//  ang-moda
//
//  Created by Salam Xiyali on 12/26/15.
//  Copyright © 2015 Salam Xiyali. All rights reserved.
//

import UIKit

class searchPageController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    let alertBox = UIAlertController(title: "Warning 1", message: "", preferredStyle: .ActionSheet)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let okAction = UIAlertAction(title: "好的", style: .Destructive, handler: nil)
        alertBox.addAction(okAction)
        
        
        searchBar.showsCancelButton = true
        searchBar.userInteractionEnabled = true //让控件接收 手势 ，某些控件默认是关闭的
        searchBar.becomeFirstResponder() //第一响应者
    }

    
    /* UISearchDelegate */

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
    }
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String){
        
        print("你输入了内容: \(searchText)")
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool{ // return NO to not become first
        // print("你开始编辑了1")
        return true
    }
    
    //    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
    //        self.setShowsCancelButton(true, animated: true)
    //        print("显示了 cancel button")
    //        return true
    //    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        // print("你点击了取消按钮")
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar){ // called when text starts editing
        // print("你开始编辑了2")
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        // print("你点击了搜索按钮")
        
        if let text = searchBar.text {
            alertBox.message = text
            
            self.presentViewController(alertBox, animated: true, completion: nil)
        }
        
    }


}
