//
//  settingPage.swift
//  ang-moda
//
//  Created by Salam Xiyali on 12/29/15.
//  Copyright © 2015 Salam Xiyali. All rights reserved.
//

import UIKit

class settingPage: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var settingTableView: UITableView!
    
    var data = [ ["name": "section One", "data": ["profile", "Yusup"]], ["name" : "hehe 22222" , "data": ["Geek" , "About"] ] ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        settingTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    /* UITableViewDataSource - cell */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return data[section]["data"]!.count
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("settingCell", forIndexPath: indexPath)
        
        if let label = cell.viewWithTag(20){
            (label as! UILabel).text = data[indexPath.section]["data"]![indexPath.row] as? String
        }
        
        return cell
    }

    /* UITableViewDataSource - section */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return data.count
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return data[section]["name"] as? String
    }


}
