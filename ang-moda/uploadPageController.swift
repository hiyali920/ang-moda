//
//  uploadPageController.swift
//  ang-moda
//
//  Created by Salam Xiyali on 12/27/15.
//  Copyright © 2015 Salam Xiyali. All rights reserved.
//

import UIKit

class uploadPageController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate{
    
    var dataSource: Array<Int> = [1,2,3,1,2,3,1,2,3,4567,568]
    
    @IBOutlet weak var imageCollection: UICollectionView!
    
    @IBOutlet weak var descTextView: UITextView!
    var descTextViewInitColor: UIColor?
    var descTextViewInitText: String?
    var descFirstChanged = false
    
    @IBAction func backBtn(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        imageCollection.dataSource = self
        imageCollection.delegate = self
        imageCollection.registerClass(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "collectionHeader")
        // imageCollection.registerClass(nil, forCellWithReuseIdentifier: "collectionHeader")
        
        descTextViewInitColor = descTextView.textColor
        descTextViewInitText = descTextView.text
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* UITextViewDelegate */
    
    func textViewDidBeginEditing(textView: UITextView) {
        if !descFirstChanged{
            descTextView.textColor = UIColor.blackColor()
            descTextView.text = ""
            descFirstChanged = true
        }
    }
    
    func textViewDidEndEditing(textView: UITextView){
        print(" textView did end changed")
        if descTextView.text == "" {
            descTextView.text = descTextViewInitText
            descTextView.textColor = descTextViewInitColor
            descFirstChanged = false
        }
    }

    /* UICollectionViewDataSource - cell */
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as UICollectionViewCell
        // cell.removeFromSuperview()

        if let label = cell.viewWithTag(10) {
            (label as! UILabel).text = "\(indexPath.item)"
            // cell.contentView.addSubview(label)
        }

        if indexPath.item == dataSource.count - 1 {
            let plusBtn = collectionView.dequeueReusableCellWithReuseIdentifier("collectionPlus", forIndexPath: indexPath) as UICollectionViewCell
            collectionView.addSubview(plusBtn)
        }
 
        return cell // return a cell for index
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return dataSource.count // return cells count in each section
    }
    
    
    /* UICollectionViewDataSource - section */
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView{

        print("get UIcollection Header begin")
        
        var reusableView: UICollectionReusableView?
        
        if (kind == UICollectionElementKindSectionHeader){
            
            print("get section header")
            
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "collectionHeader", forIndexPath: indexPath) as UICollectionReusableView
            
            if let label = header.viewWithTag(9) {
                (label as! UILabel).text = "section :: \(indexPath.section)"
                // header.addSubview(label) // just use subview from coding
                print("get section success : \(indexPath.section)")
            }
            reusableView = header
        }
        
        if (kind == UICollectionElementKindSectionFooter){
            print("get section footer")
            
            let footerview = collectionView.dequeueReusableCellWithReuseIdentifier("collectionHeader", forIndexPath: indexPath) as UICollectionReusableView
            reusableView = footerview
        }
        
        return reusableView!
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        print("count of section 1")
        return 1
    }

    /* UICollectionViewDelegate */
    


}
