//
//  ResponseDataStruct.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/20/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import Foundation
import JSONJoy

// data structure
struct AMData {
    var authToken: String?
    var refreshToken: String?
    
    init() {
    }
    
    init(_ decoder: JSONDecoder) {
        authToken = decoder["auth-token"].string
        refreshToken = decoder["refresh-token"].string
    }
}

struct RSData {
    var success: Bool?
    var message: String?
    var data: AMData?
    
    init() {
    }
    
    init(_ decoder: JSONDecoder) {
        message = decoder["message"].string
        data = AMData(decoder["token-pair"])
    }
}

// array data
//    struct Addresses : JSONJoy {
//        var addresses: Array<Address>?
//        init() {
//        }
//        init(_ decoder: JSONDecoder) {
//            //we check if the array is valid then alloc our array and loop through it, creating the new address objects.
//            if let addrs = decoder["addresses"].array {
//                addresses = Array<Address>()
//                for addrDecoder in addrs {
//                    addresses.append(Address(addrDecoder))
//                }
//            }
//        }
//    }