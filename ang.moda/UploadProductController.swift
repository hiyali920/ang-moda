//
//  UploadProductController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/20/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit
import RSKImageCropper

class UploadProductController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func choosePhoto(sender: UIButton) {
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .PhotoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        
        self.presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    /* image picker delegate */
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        // The info dictionary contains multiple representations of the image, and this uses the original.
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let imageCropVC: RSKImageCropViewController = RSKImageCropViewController(image: selectedImage)
        imageCropVC.delegate = self
        self.navigationController!.pushViewController(imageCropVC, animated: true)
        
        // Dismiss the picker.
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /* image cropper delegate */
    
    // Crop image has been canceled.
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    // The original image has been cropped.
    func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        self.imageView.image = croppedImage
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    // The original image has been cropped. Additionally provides a rotation angle used to produce image.
    func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.imageView.image = croppedImage
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    // The original image will be cropped.
    func imageCropViewController(controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        // Use when `applyMaskToCroppedImage` set to YES.
//        SVProgressHUD.show()
    }
    
    /* image cropper datasource */
    
    // Returns a custom rect for the mask.
    func imageCropViewControllerCustomMaskRect(controller: RSKImageCropViewController) -> CGRect {
        var maskSize: CGSize
        if controller.isPortraitInterfaceOrientation() {
            maskSize = CGSizeMake(250, 250)
        }
        else {
            maskSize = CGSizeMake(220, 220)
        }
        let viewWidth: CGFloat = CGRectGetWidth(controller.view.frame)
        let viewHeight: CGFloat = CGRectGetHeight(controller.view.frame)
        let maskRect: CGRect = CGRectMake((viewWidth - maskSize.width) * 0.5, (viewHeight - maskSize.height) * 0.5, maskSize.width, maskSize.height)
        return maskRect
    }
    
    // Returns a custom path for the mask.
    func imageCropViewControllerCustomMaskPath(controller: RSKImageCropViewController) -> UIBezierPath {
        let rect: CGRect = controller.maskRect
        let point1: CGPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect))
        let point2: CGPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))
        let point3: CGPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect))
        let triangle: UIBezierPath = UIBezierPath()
        triangle.moveToPoint(point1)
        triangle.addLineToPoint(point2)
        triangle.addLineToPoint(point3)
        triangle.closePath()
        return triangle
    }
    
    // Returns a custom rect in which the image can be moved.
    func imageCropViewControllerCustomMovementRect(controller: RSKImageCropViewController) -> CGRect {
        // If the image is not rotated, then the movement rect coincides with the mask rect.
        return controller.maskRect
    }
}
