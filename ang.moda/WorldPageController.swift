//
//  WorldPageController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/21/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

import CollectionViewWaterfallLayout
import Kingfisher

private let reuseIdentifier = "worldProductItemCell"

class WorldPageController: UIViewController, UICollectionViewDataSource, CollectionViewWaterfallLayoutDelegate {
    
    @IBOutlet var collectionView: UICollectionView!
    var aInt:Int = 0
    
    func load_image(imgURL:NSURL)
    {
        
//        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        NSURLConnection.sendAsynchronousRequest(
            request, queue: NSOperationQueue.mainQueue(),
            completionHandler: {(response: NSURLResponse?,data: NSData?,error: NSError?) -> Void in
                if error == nil {
                    let image = UIImage(data: data!)
                    print("download image:  \(image)")
                     self.cellSizes.append((image?.size)!)
                     self.dataSource.append(imgURL)
                     self.collectionView.reloadData()
                }
        })
        
    }
    
    @IBAction func addButton(sender: UIButton) {
        print("cellsize count \(cellSizes.count)")
        
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "mm:ss.SSS"
        formatter.stringFromDate(currentDateTime)
        print("add button click: \(formatter.dateFormat)")
        
        load_image(dataSource[aInt % dataSource.count])
        aInt++
    }
    
    var dataSource: Array<NSURL> = [ NSURL(string: "http://s15.sinaimg.cn/mw690/4b92d607gde90812ea74e&690")!, NSURL(string: "http://www.qqleju.com/uploads/allimg/121226/1-121226120156.jpg")!, NSURL(string: "http://pic18.nipic.com/20111211/9012800_203456084332_2.jpg")! ]
    var itemWidth: CGFloat!
    
    /* waterfall layout delegate */
    lazy var cellSizes: [CGSize] = {
        var _cellSizes = [CGSize]()
        
        for _ in 0...2 {
            let random = Int(arc4random_uniform((UInt32(100))))
            let productWidth = Int(self.view.frame.size.width / 2 - 20)
            _cellSizes.append(CGSize(width: productWidth, height: 50 + random))
        }
        
        return _cellSizes
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.headerInset = UIEdgeInsetsMake(20, 0, 0, 0)
        layout.headerHeight = 50
        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        collectionView.collectionViewLayout = layout
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! WorldPageItemCell
        let dataIndex = indexPath.item % dataSource.count;
        cell.productImage.kf_setImageWithURL(dataSource[dataIndex], placeholderImage: UIImage(named: "shareButtonIcon"))
        return cell

    }
    
    // MARK: WaterfallLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return cellSizes[indexPath.item]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
