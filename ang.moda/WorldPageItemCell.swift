//
//  WorldPageItemCell.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/21/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

class WorldPageItemCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
}
