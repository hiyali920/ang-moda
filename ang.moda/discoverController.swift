//
//  discoverController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/3/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit
import Kingfisher

class discoverController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate{
    
    @IBOutlet weak var classificTableView: UITableView!
    
    let tableCellIdentifier = "tableCell"
    let collectionCellIdentifier = "collectionCell"
    
    var productPage: UIViewController!
    
    let dataSource: Array<NSURL> = [ NSURL(string: "http://s15.sinaimg.cn/mw690/4b92d607gde90812ea74e&690")!, NSURL(string: "http://www.qqleju.com/uploads/allimg/121226/1-121226120156.jpg")!, NSURL(string: "http://pic18.nipic.com/20111211/9012800_203456084332_2.jpg")! ]

    @IBAction func clickAProduct(sender: UIButton) {
        self.productPage.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(self.productPage, animated: true)
        self.productPage.hidesBottomBarWhenPushed = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // init product page
        prodcutPageInit()

        self.classificTableView.dataSource = self
        self.classificTableView.delegate = self
        
//        swipeInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    /* UICollectionViewDataSource - cell */
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
//        var cell = collectionView.cellForItemAtIndexPath(indexPath) //准确地取出一行，而不是从cell重用队列中
        
        let productItem = collectionView.dequeueReusableCellWithReuseIdentifier(collectionCellIdentifier, forIndexPath: indexPath) as! discoverItemCollectionViewCell
        
//        let productItem = cell as! discoverItemCollectionViewCell
        productItem.discoverItemImage.kf_setImageWithURL(dataSource[indexPath.item], placeholderImage: UIImage(named: "shareButtonIcon"))
//        productItem.discoverItemTitle.text = "\(dataSource[indexPath.item])"
//        productItem.discoverItemDesc.text = "\(dataSource[indexPath.item]) Desc"
        
        return productItem // return a cell for index
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return dataSource.count // return cells count in each section
    }
    
    
    /* tableViewDataSource - cell */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1 // dataSource.count
    }
    
    // for hide the separator last one
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath){
        if indexPath.row == 1 {
            cell.separatorInset = UIEdgeInsetsMake(0.0, cell.bounds.size.width, 0.0, 0.0)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{

//        let cell = tableView.cellForRowAtIndexPath(indexPath) as! discoverSectionTableViewCell //准确地取出一行，而不是从cell重用队列中
        let cell = classificTableView.dequeueReusableCellWithIdentifier(tableCellIdentifier, forIndexPath: indexPath) as! discoverSectionTableViewCell
        cell.itemsCollectionView.delegate = self
        cell.itemsCollectionView.dataSource = self
        
//        cell.itemsCollectionView.tag = indexPath.row
        
//        cell.autoresizingMask = UIViewAutoresizing.FlexibleHeight// UIViewAutoresizingFlexibleHeight
//        cell.clipsToBounds = true
        
        return cell // return section
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return dataSource.count
    }
    
    /* UITableView Header*/
    
//    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Section \(section)"
//    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = UIView()
//        vw.backgroundColor = UIColor.blackColor()
//        
//        return vw
//    }
    
    /* product page */
    
    func prodcutPageInit(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle:nil);
        productPage = sb.instantiateViewControllerWithIdentifier("productPage")
    }
    

    /* Swipe */
    
//    func swipeInit(){
//        let directions: [UISwipeGestureRecognizerDirection] = [.Right, .Left, .Up, .Down]
//        for direction in directions {
//            let gesture = UISwipeGestureRecognizer(target: self, action: Selector("swipeGestureHandle:"))
//            gesture.direction = direction
//            self.view.addGestureRecognizer(gesture)
//        }
//    }
//    
//    func swipeGestureHandle(gesture: UIGestureRecognizer) {
//        
//        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
//            switch swipeGesture.direction {
//            case UISwipeGestureRecognizerDirection.Right:
//                print("Swiped right")
//            case UISwipeGestureRecognizerDirection.Left:
//                ()
//            case UISwipeGestureRecognizerDirection.Down:
//                print("Swiped down")
//            case UISwipeGestureRecognizerDirection.Up:
//                print("Swiped up")
//            default:
//                break
//            }
//        }
//    }

}
