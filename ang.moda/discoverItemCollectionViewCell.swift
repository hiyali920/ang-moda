//
//  discoverItemCollectionViewCell.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/19/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

class discoverItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var discoverItemImage: UIImageView!

    @IBOutlet weak var discoverItemTitle: UILabel!

    @IBOutlet weak var discoverItemDesc: UILabel!

}
