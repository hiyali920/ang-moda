//
//  homeController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/3/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

class homeController: UIViewController {
    
    var uploadPage: UIViewController!

    @IBAction func addProductBtnClick(sender: UIButton) {
        self.uploadPage.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(uploadPage, animated: true)
        self.uploadPage.hidesBottomBarWhenPushed = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        uploadPageInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* upload page init */
    
    func uploadPageInit(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle:nil);
        uploadPage = sb.instantiateViewControllerWithIdentifier("uploadProductPage")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
