//
//  loginController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/4/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit
import SwiftHTTP
import JSONJoy

class loginController: UIViewController, UINavigationControllerDelegate {
    
    var resetPage: UIViewController!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loginAreaContent: UIView!
    let angmodaData = ShareData.sharedInstance

    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func forgetPassBtnClick(sender: UIButton) {
        self.presentViewController(resetPage, animated: true, completion: nil)
    }
    
    @IBAction func loginBtnClick(sender: UIButton) {
        
        print("loginBtnClick")
        // login
        var username = ""
        var password = ""
        
        if let text = self.usernameInput.text {
            username = text
        }
        
        if let text = self.passwordInput.text {
            password = text
        }
        
        let params = [
            "username": username,
            "password": password
        ]
        
        // post
        do {
            let opt = try HTTP.POST( angmodaData.API_LOGIN , parameters: params, headers: angmodaData.JOIN_HEADER, requestSerializer: JSONParameterSerializer())
            opt.start { response in
                
                print("login post rs: \(NSString(data: response.data, encoding: NSUTF8StringEncoding))")
                
                if let rsSuccess = RSData(JSONDecoder(response.data)).success{
                    if !rsSuccess {
                        return
                    }
                }
                
                if let authToken = RSData(JSONDecoder(response.data)).data?.authToken, let refreshToken = RSData(JSONDecoder(response.data)).data?.refreshToken{
                    print("authToken: \(authToken)")
                    print("refreshToken: \(refreshToken)")
                    self.angmodaData.authToken = authToken
                    self.angmodaData.refreshToken = refreshToken
                }
                
            }
        } catch let error {
            print("got an error creating login request: \(error)")
        }


        
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
    }
    
    @IBAction func backBtnClick(sender: UIButton) {
        closeWindow()
        print("backBtnClick")
    }
    
    func closeWindow(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        swipeInit()
        resetPageInit()
        
        scrollView.contentSize = loginAreaContent.bounds.size
        
        self.registerButton.layer.borderColor = (self.angmodaData.mainBlueColor).CGColor
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /* reset page */
    func resetPageInit(){
        let sb:UIStoryboard = UIStoryboard(name: "Main", bundle:nil);
        self.resetPage = sb.instantiateViewControllerWithIdentifier("resetPageIdentifier")
    }
    
    /* open register page */
    
    func openRegisterPage(){
        //        self.modalTransitionStyle = UIModalTransitionStyle.CoverVertical // Cover Vertical is necessary for CurrentContext
        //        self.modalPresentationStyle = .CurrentContext // Display on top of    current UIView
        //        let regPage:UIViewController  = regController()
        
        //        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        //        let vc : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("regController") as UIViewController
        
        //        self.presentViewController(regPage, animated: true, completion: nil)
        
        let regController = (self.storyboard?.instantiateViewControllerWithIdentifier("regControllerIdentifier"))! as UIViewController
        //        self.navigationController?.pushViewController(regControllerObejct!, animated: true)
        
        self.presentViewController(regController, animated: true, completion: nil)
        
    }

    /* Swipe */
    
    func swipeInit(){
        let directions: [UISwipeGestureRecognizerDirection] = [.Right, .Left, .Up, .Down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: Selector("swipeGestureHandle:"))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }
    
    func swipeGestureHandle(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                openRegisterPage()
            case UISwipeGestureRecognizerDirection.Down:
                closeWindow()
            case UISwipeGestureRecognizerDirection.Left:
                openRegisterPage()
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }

}
