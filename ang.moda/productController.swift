//
//  productController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/6/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

class productController: UIViewController, SlickViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var contentView: UIView!
    
    var slickView: SlickView?
    
    @IBAction func backBtnClick(sender: UIButton) {
        closeWindow()
    }
    
    func closeWindow(){
        // self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // self.navigationController?.interactivePopGestureRecognizer?.enabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        slickInit()
        swipeInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        slickView!.removeFromSuperview()
        slickView!.delete(slickView)
    }
    
    /* Orientation */
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation){
        
        // have an issue , the current index replace with initial index...
        slickView!.removeFromSuperview()
        
        slickInit()
    }

    
    /* Swipe */
    
    func swipeInit(){
        let directions: [UISwipeGestureRecognizerDirection] = [.Right, .Left, .Up, .Down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: Selector("swipeGestureHandle:"))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }

    func swipeGestureHandle(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.Right:
                    print("Swiped right")
                case UISwipeGestureRecognizerDirection.Down:
                    closeWindow()
                case UISwipeGestureRecognizerDirection.Left:
                    print("Swiped left")
                case UISwipeGestureRecognizerDirection.Up:
                    print("Swiped up")
                default:
                    break
            }
        }
    }
    

    /* SlickViewDelegate */
    
    // init
    
    func slickInit(){
//        let frame: CGRect = self.contentView.bounds
        let frame: CGRect = CGRect(x: 0.0, y: 0.0, width: self.contentView.frame.width, height: 300)
        let images: Array<NSURL> = [ NSURL(string: "http://s15.sinaimg.cn/mw690/4b92d607gde90812ea74e&690")!, NSURL(string: "http://www.qqleju.com/uploads/allimg/121226/1-121226120156.jpg")!, NSURL(string: "http://pic18.nipic.com/20111211/9012800_203456084332_2.jpg")! ]
        
        slickView = SlickView(frame: frame, placeholderImage: UIImage(named: "userTabIcon")!, URLArr: images)
        slickView!.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(slickView!)

        // self.view.bringSubviewToFront(playView)
        
        // auto Layout
        
//        self.view.superview!.translatesAutoresizingMaskIntoConstraints = false
        let constTop:NSLayoutConstraint = NSLayoutConstraint(item: slickView!, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.contentView, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        let constLeft:NSLayoutConstraint = NSLayoutConstraint(item: slickView!, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.contentView, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0)
        let constRight:NSLayoutConstraint = NSLayoutConstraint(item: slickView!, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: self.contentView, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: 0)
        let constHeight:NSLayoutConstraint = NSLayoutConstraint(item: slickView!, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: frame.height)
//        let constHeight = NSLayoutConstraint(item: playView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 400)
        
        // if you need this contraints
        // let widthConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
        
        self.contentView.addConstraint(constTop)
        self.contentView.addConstraint(constLeft)
        self.contentView.addConstraint(constRight)
        self.contentView.addConstraint(constHeight)
        
        // imgView.kf_setImageWithURL(NSURL(string: product.pictureUrl)!, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, CacheType, imageURL) -> () in
        // self.ProductTable.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)})
        
    }
    
    func slickView(SlickView: UIView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        debugPrint("indexPath: \(indexPath)")
    }

}
