//
//  regController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/5/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import UIKit

class regController: UIViewController, UIScrollViewDelegate {
    let angmodaData = ShareData.sharedInstance

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var registerAreaContent: UIView!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func backBtnClick(sender: UIButton) {
        closeWindow()
    }
    @IBAction func regBtnClick(sender: UIButton) {
        closeWindow()
    }
    
    func closeWindow(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        swipeInit()
        
        scrollView.contentSize = registerAreaContent.bounds.size
        
        self.loginButton.layer.borderColor = (self.angmodaData.mainBlueColor).CGColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /* Swipe */
    
    func swipeInit(){
        let directions: [UISwipeGestureRecognizerDirection] = [.Right, .Left, .Up, .Down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: Selector("swipeGestureHandle:"))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }
    
    func swipeGestureHandle(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                closeWindow()
            case UISwipeGestureRecognizerDirection.Left:
                closeWindow()
            case UISwipeGestureRecognizerDirection.Down:
                () // closeWindow() // can close self and the login page?
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }

}
