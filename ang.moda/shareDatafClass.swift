//
//  shareDatafClass.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/19/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

import Foundation

import KeychainSwift

class ShareData: UIViewController {
    let keychain = KeychainSwift(keyPrefix: "angmoda_")
    let alertBox = UIAlertController(title: "Warning", message: "", preferredStyle: .ActionSheet)

    class var sharedInstance: ShareData {
        struct Static {
            static var instance: ShareData?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ShareData()
        }
        
        return Static.instance!
    }
    
    
    var authToken :String? {
        get {
            return keychain.get("auth_token")
        }
        set(token) {
            if keychain.set(token!, forKey: "auth_token") {
                // Keychain item is saved successfully
            }else{
                let text = "登入出错，请重试：00_1"
                alertBox.message = text
                self.presentViewController(alertBox, animated: true, completion: nil)
            }
        }
    }
    
    var refreshToken : String? {
        get {
            return keychain.get("refresh_token")
        }
        set(token) {
            if keychain.set(token!, forKey: "refresh_token") {
                // Keychain item is saved successfully
            }else{
                let text = "登入出错，请重试：00_2"
                alertBox.message = text
                self.presentViewController(alertBox, animated: true, completion: nil)
            }
        }
    }
    
    var userAccount : String{
        get {
            var result = ""
            let token = keychain.get("user_account")
            if token != nil{
                result = token!
            }
            return result
        }
        set(token) {
            if keychain.set(token, forKey: "user_account") {
                // Keychain item is saved successfully
            }else{
                let text = "用户名保存出错，请联系管理员：00_3"
                alertBox.message = text
                self.presentViewController(alertBox, animated: true, completion: nil)
            }
        }
    }
    
    let mainBlueColor: UIColor = UIColor(red: 58/255, green: 143/255, blue: 210/255, alpha: 1)

    
    let API_LOGIN = "https://wef84tioi5.execute-api.ap-northeast-1.amazonaws.com/prod01"
    let API_REG = "https://7rolzya2oe.execute-api.ap-northeast-1.amazonaws.com/prod01"
    
    let JOIN_HEADER = [
        "x-api-key": "rtunWBiiLu8LMe8hOOZjk1fPEtqMFQ9lahFgMvFv",
        "Content-Type": "application/json"
    ]
    

}

