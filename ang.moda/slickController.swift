//
//  slickController.swift
//  ang.moda
//
//  Created by Salam Xiyali on 1/6/16.
//  Copyright © 2016 Salam Xiyali. All rights reserved.
//

//
//  SlickView.swift
//  SlickView
//
//  Created by Broccoli on 15/11/16.
//  Copyright © 2015年 Broccoli. All rights reserved.
//

import UIKit
import Kingfisher

protocol SlickViewDelegate {
    func slickView(SlickView: UIView, didSelectItemAtIndexPath indexPath: NSIndexPath)
}

class SlickView: UIView {
    
    /// 轮播间隔时间 默认是 5 秒
    var switchInterval: Double! {
        set {
            switchTimer = NSTimer.scheduledTimerWithTimeInterval(newValue, target: self, selector: Selector("beganSwitchAnimation"), userInfo: nil, repeats: true)
            switchTimer.fireDate = NSDate(timeIntervalSinceNow: newValue)
        }
        
        get {
            return nil
        }
    }
    
    private lazy var collectionView: UICollectionView! = {
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = self.frame.size
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        let collection = UICollectionView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: self.frame.size), collectionViewLayout: layout)

        collection.pagingEnabled = true
        collection.bounces = false
        collection.delegate = self
        collection.dataSource = self
        collection.showsHorizontalScrollIndicator = false
        collection.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: CellIdentifier)
        collection.scrollToItemAtIndexPath(NSIndexPath(forItem: 1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
        collection.contentOffset = CGPoint(x: 0, y: 0)

        return collection

    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = self.imgURLArr.count
        pageControl.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 10)
        pageControl.center = CGPoint(x: self.frame.size.width / 2.0, y: self.frame.size.height - 20)
        return pageControl
    }()
    
    
    /// 图片 URL 数组
    var imgURLArr: [NSURL]!
    /// placeholder image
    var placeholderImage: UIImage!
    /// 滚动 时间间隔
    private var switchTimer: NSTimer!
    /// 一个 当前图片 的变量 用于 计算 collectionView 的偏移
    private var dataCurrentIndex = 0
    /// pageControl 的 currentPage
    private var pageControlCurrent: Int!
    /// 点击事件的 回调
    // block
    var didSelectItemAtIndexBlock: ((NSIndexPath) -> Void)!
    // delegate
    var delegate: SlickViewDelegate!
    
    // MARK: - init
    init(frame: CGRect, placeholderImage: UIImage, URLArr: [NSURL]) {
        super.init(frame: frame)
        self.imgURLArr = URLArr
        addSubview(collectionView)
        insertSubview(pageControl, aboveSubview: self.collectionView)
        self.placeholderImage = placeholderImage
        self.switchInterval = 5.0
        
//        let constrainsItems: [NSLayoutAttribute] = [.Top, .Bottom, .Left, .Right]
//        collectionView.translatesAutoresizingMaskIntoConstraints = false
//        for constrainsItem in constrainsItems{
//            let constrains = NSLayoutConstraint(item: collectionView, attribute: constrainsItem, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: constrainsItem, multiplier: 1, constant: 0)
//            self.addConstraint(constrains)
//        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - 页面 滚动
extension SlickView {
    private func correctCollectionViewOffset(scrollView: UIScrollView) {
        

        let offset = scrollView.contentOffset.x / scrollView.bounds.width - 1
        
        if offset != 0 {
            
            dataCurrentIndex = (dataCurrentIndex + Int(offset) + imgURLArr.count) + imgURLArr.count
            
            let indexPath = NSIndexPath(forItem: 1, inSection: 0)
            
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
            
            UIView.setAnimationsEnabled(false)
            collectionView.reloadItemsAtIndexPaths([indexPath])
            UIView.setAnimationsEnabled(true)
            
            pageControl.currentPage = pageControlCurrent
        }
    }
    
    @objc private func beganSwitchAnimation() {
        let indexPath = NSIndexPath(forItem: 2, inSection: 0)
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        
    }
    

}

public let CellIdentifier = "slickViewCell"
// MARK: - UICollectionViewDataSource
extension SlickView: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.cellForItemAtIndexPath(indexPath) //准确地取出一行，而不是从cell重用队列中
        if cell == nil {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellIdentifier, forIndexPath: indexPath)
            cell!.backgroundColor = UIColor.whiteColor()
            cell!.viewWithTag(5)
            cell!.frame.origin.y = 0.0
            cell!.frame.size.width = self.frame.width
        }
        
        let url = imgURLArr[(indexPath.row - 1 + imgURLArr.count + dataCurrentIndex) % imgURLArr.count]
        pageControlCurrent = (indexPath.row - 1 + imgURLArr.count + dataCurrentIndex) % imgURLArr.count
        
        if let imgView = cell!.viewWithTag(10) {
            (imgView as! UIImageView).kf_setImageWithURL(url, placeholderImage: placeholderImage)
             // imgView.frame.origin.y = 0.0
             // imgView.frame.size.width = self.frame.width
        }else{
            let imgView = UIImageView(frame: bounds)
            imgView.tag = 10
            imgView.kf_setImageWithURL(url, placeholderImage: placeholderImage)
            imgView.contentMode = UIViewContentMode.ScaleAspectFill
            
            cell!.addSubview(imgView)
        }
        
        
        return cell!
    }
}

// MARK: - UICollectionViewDelegate
extension SlickView: UICollectionViewDelegate {
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
        if let _ = switchTimer {
            switchTimer.fireDate = NSDate.distantFuture()
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let _ = switchTimer {
            switchTimer.fireDate = NSDate(timeIntervalSinceNow: switchTimer.timeInterval)
        }
    }
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        correctCollectionViewOffset(scrollView)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        correctCollectionViewOffset(scrollView)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let block = didSelectItemAtIndexBlock {
            block(NSIndexPath(forItem: pageControlCurrent, inSection: 0))
        }
        
        if let delegate = delegate {
            delegate.slickView(self, didSelectItemAtIndexPath: NSIndexPath(forItem: pageControlCurrent, inSection: 0))
        }
    }
    
}



